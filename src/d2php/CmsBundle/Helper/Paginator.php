<?php

namespace d2php\CmsBundle\Helper;

use Doctrine\Common\Collections\Collection;

class Paginator {
	/**
	 * @var Doctrine\Common\Collections\Collection
	 */
	private $objects;
	/**
	 * @var integer
	 */
	private $nbObjects;
	/**
	 * @var integer
	 */
	private $maxObjPerPage;
	/**
	 * @var integer
	 */
	private $nbPages;
	/**
	 * @var integer
	 */
	private $currentPage;
	
	/**
	 * @param Collection $objects
	 * @param integer $maxObjPerPage
	 * @param integer $currentPage
	 */
	function __construct(Collection $objects, $maxObjPerPage, $currentPage = 1){
		$this->objects = $objects;
		$this->nbObjects = $this->objects->count();
		$this->maxObjPerPage = $maxObjPerPage;
		$this->nbPages = intval($this->nbObjects / $this->maxObjPerPage + ($this->nbObjects % $this->maxObjPerPage ? 1 : 0));
		$this->currentPage = $currentPage;
	}
	
	/**
	 * @param integer $currentPage
	 */
	public function setCurrentPage($currentPage){
		$this->currentPage = $currentPage;
	}
	
	/**
	 * @return integer
	 */
	public function getCurrentPage(){
		return $this->currentPage;
	}
	
	/**
	 * @return array
	 */
	public function objects(){
		$start = ($this->currentPage - 1) * $this->maxObjPerPage;
		$end = $start + $this->maxObjPerPage;
		
		$ret = array();
		for ($i = $start; $i < $end; $i++){
			if ($this->objects[$i]){
				$ret[] = $this->objects[$i];
			}
		}
		
		return $ret;
	}
	
	private function validPageId($pageId){
		if ($pageId < 0){
			$pageId = 0;
		}
		if ($pageId > $this->nbPages){
			$pageId = $this->nbPages;
		}
		return $pageId;
	}
	
	public function renderLinks(){
		if ($this->nbPages <= 1){
			return '1';
		}
		
		$range = 2;
		
		$previousPageId = $this->validPageId($this->currentPage - 1);
		$nextPageId = $this->validPageId($this->currentPage + 1);
		
		$ret = '';
		$ret.= '<a href="?page='. $previousPageId .'">Previous</a> ';
		
		for ($i = $range * -1; $i <= $range; $i++){
			if ($i <= 0 || $i > $this->nbPages) continue;
			$ret.= '<a href="?page='. $i .'">'. $i .'</a> ';
		}
		
		$ret.= '<a href="?page='. $nextPageId .'">Next</a>';
		
		return $ret;
	}
}