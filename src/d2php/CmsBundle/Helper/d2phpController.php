<?php

namespace d2php\CmsBundle\Helper;

use d2php\UserBundle\d2phpUserBundle;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Form;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class d2phpController extends Controller {
	/**
	 * If POST action, bind the request and return true. Else, return false.
	 * 
	 * @param Form $form
	 * @return boolean
	 */
	protected function isPostAction(Form $form){
		if ($this->getRequest()->getMethod() == 'POST'){
			$form->bindRequest($this->getRequest());
			return true;
		}
		return false;
	}
	
	/**
	 * @return \d2php\UserBundle\Entity\User
	 */
	protected function getUser(){
		return $this->get('security.context')->getToken()->getUser();
	}
	
	public function render($view, array $parameters = array(), Response $response = null){
		return parent::render(
				$view, 
				array_merge(
					$parameters,
					array(
							'nb_users' => $this->getDoctrine()->getRepository('d2phpUserBundle:User')->count(),
							'servers' => $this->get('d2php.server_manager')->getServers(),
							'lien_vote' => $this->getParameter('lien_vote'),
					)
				),
				$response
		);	
	}
	
	public function getParameter($name){
		return $this->container->getParameter($name);
	}
}