<?php

namespace d2php\CmsBundle\Controller;

use d2php\CmsBundle\d2phpCmsBundle;

use d2php\UserBundle\d2phpUserBundle;

use d2php\NewsBundle\d2phpNewsBundle;

use Doctrine\Common\Collections\ArrayCollection;

use d2php\CmsBundle\Helper\Paginator;
use d2php\CmsBundle\Helper\d2phpController;

DEFINE('MAX_NEWS_PER_PAGE', 5);

class DefaultController extends d2phpController
{
	public function homeAction(){
		$news = new Paginator(
				new ArrayCollection($this->getDoctrine()->getRepository('d2phpNewsBundle:News')->findAllDesc()), 
				MAX_NEWS_PER_PAGE,
				$this->getRequest()->query->get('page', 1)
		);
		return $this->render('d2phpCmsBundle:Default:home.html.twig', array(
				'news' => $news
		));
	}
	
	public function adminAction(){
		return $this->render('d2phpCmsBundle:Default:admin.html.twig');
	}
	
	public function joinusAction(){
		return $this->render('d2phpCmsBundle:Default:joinus.html.twig');
	}
	
	public function staffAction(){
		$members = $this->getDoctrine()->getRepository('d2phpUserBundle:User')->getStaff();
		return $this->render('d2phpCmsBundle:Default:staff.html.twig', array(
				'members' => $members
		));
	}
	
	public function infosAction(){
		return $this->render('d2phpCmsBundle:Default:infos.html.twig');
	}
}
