<?php

namespace d2php\CmsBundle\Service;

use Symfony\Bundle\DoctrineBundle\Registry;

class ServerManager {
	public static function statut($IP, $PORT)
	{
		if (@fsockopen($IP, $PORT, $ERRNO, $ERRSTR, 0.5 )){
			$statut = TRUE;
		}
		else{
			$statut = FALSE;
		}
		@fclose();
		return $statut;
	}
	
	/**
	 * @var Registry
	 */
	private $doctrine;
	
	public function __construct(Registry $doctrine){
		$this->doctrine = $doctrine;	
	}
	
	/**
	 * @return array
	 */
	public function getServers(){
		return array(
				array(
						'id'         => 1,
						'name'       => 'Hélios',
						'is_online'  => ServerManager::statut('91.229.20.145', '4444'),
						'players'    => $this->doctrine->getRepository('d2phpUserBundle:User')->getNbConnected(),
						'characters' => $this->doctrine->getRepository('d2phpUserBundle:Player')->getNbPlayers()
				)
		);
	}
}