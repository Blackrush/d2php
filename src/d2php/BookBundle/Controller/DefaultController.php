<?php

namespace d2php\BookBundle\Controller;

use Symfony\Component\Form\FormError;

use Doctrine\Common\Collections\ArrayCollection;

use d2php\CmsBundle\Helper\Paginator;
use d2php\CmsBundle\Helper\d2phpController;
use d2php\BookBundle\Entity\Message;
use d2php\BookBundle\Form\MessageType;

define('MAX_MSG_PER_PAGE', 20);

class DefaultController extends d2phpController {
	public function viewAction(){
		$repo = $this->getDoctrine()->getRepository('d2phpBookBundle:Message');
		
		$messages = new Paginator(
				new ArrayCollection($repo->findAll()), 
				MAX_MSG_PER_PAGE,
				$this->getRequest()->query->get('page', 1)
		);
		$average = $repo->getMarkAvg();
		
		return $this->render('d2phpBookBundle:Default:view.html.twig', array(
				'messages' => $messages,
				'average' => $average
		));
	}
	
	public function addAction(){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return $this->redirect($this->generateUrl('d2php_home'));
		}
		
		$repo = $this->getDoctrine()->getRepository('d2phpBookBundle:Message');
		$user = $this->getUser();
		
		$message = new Message;
		$form = $this->createForm(new MessageType, $message);
		
		if ($this->isPostAction($form) && $form->isValid()){
			if ($repo->alreadyAddMessage($user)){
				$form->addError(new FormError('Vous avez déjà poster un message.'));	
			}
			else{
				$message->setAuthor($user);
				$message->setDate(new \DateTime);
				
				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($message);
				$em->flush();
				
				return $this->redirect($this->generateUrl('d2php_book_view'));
			}
		}
		
		return $this->render('d2phpBookBundle:Default:add.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function deleteAction(Message $message){
		if (!$this->get('security.context')->isGranted('ROLE_GAME_MASTER')){
			throw $this->createNotFoundException();
		}
		
		if ($this->getRequest()->query->get('confirmation', false)){
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($message);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_admin'));
		}
		
		return $this->render('d2phpBookBundle:Default:delete.html.twig', array(
				'message' => $message
		));
	}
}
