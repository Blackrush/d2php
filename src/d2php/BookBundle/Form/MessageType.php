<?php

namespace d2php\BookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('mark', 'integer', array(
            		'label' => 'Entrez une note de 0 à 20 : '
            ))
            ->add('message', 'textarea', array(
            		'label' => 'Entrez votre appréciation : '
           	))
        ;
    }

    public function getName()
    {
        return 'd2php_bookbundle_messagetype';
    }
}
