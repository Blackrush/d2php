<?php

namespace d2php\BookBundle\Entity;

use d2php\UserBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * d2php\BookBundle\Entity\Message
 *
 * @ORM\Table(name="d2php_messages")
 * @ORM\Entity(repositoryClass="d2php\BookBundle\Entity\MessageRepository")
 */
class Message
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var d2php\UserBundle\Entity\User $author
     *
     * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="guid")
     */
    private $author;

    /**
     * @var integer $mark
     *
     * @ORM\Column(name="mark", type="integer")
	 * @Assert\Min(limit = "0", message = "La note doit être positive ou nulle.")
	 * @Assert\Max(limit = "20", message = "La note doit être inférieur ou égale à 20.")
     */
    private $mark;

    /**
     * @var text $message
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var datetime $date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param \d2php\UserBundle\Entity\User $author
     */
    public function setAuthor(\d2php\UserBundle\Entity\User $author){
    	$this->author = $author;
    }
    
    /**
     * @return \d2php\UserBundle\Entity\User
     */
    public function getAuthor(){
    	return $this->author;
    }

    /**
     * Set mark
     *
     * @param integer $mark
     */
    public function setMark($mark)
    {
        $this->mark = $mark;
    }

    /**
     * Get mark
     *
     * @return integer 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set message
     *
     * @param text $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get message
     *
     * @return text 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return datetime 
     */
    public function getDate()
    {
        return $this->date;
    }
}