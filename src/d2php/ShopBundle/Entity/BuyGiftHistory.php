<?php

namespace d2php\ShopBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use d2php\UserBundle\Entity\User;

/**
 * d2php\ShopBundle\Entity\BuyGiftHistory
 *
 * @ORM\Table(name="d2php_gifts_buy_history")
 * @ORM\Entity
 */
class BuyGiftHistory {
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var User $buyer
	 *
	 * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="buyer_id", referencedColumnName="id")
	 */
	private $buyer;

	/**
	 * @var integer $cost
	 * 
	 * @ORM\Column(name="cost", type="integer")
	 */
	private $cost;

	/**
	 * @var datetime $date
	 * 
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return User
	 */
	public function getBuyer() {
		return $this->buyer;
	}

	/**
	 * @param User $buyer
	 */
	public function setBuyer(User $buyer) {
		$this->buyer = $buyer;
	}

	/**
	 * @return integer
	 */
	public function getCost() {
		return $this->cost;
	}

	/**
	 * @param integer $cost
	 */
	public function setCost($cost) {
		$this->cost = $cost;
	}

	/**
	 * @return datetime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param datetime $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

}
