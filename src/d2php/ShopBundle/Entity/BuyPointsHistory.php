<?php

namespace d2php\ShopBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use d2php\UserBundle\Entity\User;

/**
 * d2php\ShopBundle\Entity\BuyPointsHistory
 *
 * @ORM\Table(name="d2php_points_history")
 * @ORM\Entity
 */
class BuyPointsHistory {
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var User $buyer
	 *
	 * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="buyer_id", referencedColumnName="id")
	 */
	private $buyer;

	/**
	 * @var integer $quantity
	 *
	 * @ORM\Column(name="quantity", type="integer")
	 */
	private $quantity;

	/**
	 * @var datetime $date
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return User
	 */
	public function getBuyer() {
		return $this->buyer;
	}

	/**
	 * @param User $buyer
	 */
	public function setBuyer(User $buyer) {
		$this->buyer = $buyer;
	}

	/**
	 * Set quantity
	 *
	 * @param integer $quantity
	 */
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}

	/**
	 * Get quantity
	 *
	 * @return integer 
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * Set date
	 *
	 * @param datetime $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * Get date
	 *
	 * @return datetime 
	 */
	public function getDate() {
		return $this->date;
	}

}
