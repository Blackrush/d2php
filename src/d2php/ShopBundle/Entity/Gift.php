<?php

namespace d2php\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\ShopBundle\Entity\Gift
 *
 * @ORM\Table(name="d2php_gifts")
 * @ORM\Entity(repositoryClass="d2php\ShopBundle\Entity\GiftRepository")
 */
class Gift
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer $itemId
     *
     * @ORM\Column(name="itemId", type="integer")
     */
    private $itemId;
    
    /**
     * @var Category $category
     * 
     * @ORM\ManyToOne(targetEntity="d2php\ShopBundle\Entity\Category", inversedBy="gifts")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var integer $quantity
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var integer $price
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }
    
    /**
     * Set category
     * 
     * @param Category $category
     */
    public function setCategory(Category $category){
    	$this->category = $category;
    }
    
    /**
     * Get category
     * 
     * @return Category
     */
    public function getCategory(){
    	return $this->category;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param integer $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    public function __toString(){
    	$repr = '';
    	$repr .= '<u>'. $this->name .'</u> : <b>'. $this->price .'</b> ogrines.';
    	if (!empty($this->description)){
    		$repr .= '<br/><u>Description :</u> <i>'. $this->description .'</i>';
    	}
    	
    	return $repr;
    }
}