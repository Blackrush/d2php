<?php

namespace d2php\ShopBundle\Entity;

define('LIVE_ACTION_GIVE_ITEM', 20);

use d2php\UserBundle\Entity\Player;

use d2php\UserBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\ShopBundle\Entity\LiveAction
 *
 * @ORM\Table(name="live_action")
 * @ORM\Entity(repositoryClass="d2php\ShopBundle\Entity\LiveActionRepository")
 */
class LiveAction
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Player $player
     * 
     * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\Player")
     * @ORM\JoinColumn(name="PlayerID", referencedColumnName="guid")
     */
    private $player;

    /**
     * @var integer $action
     *
     * @ORM\Column(name="Action", type="integer")
     */
    private $action;

    /**
     * @var integer $argument
     *
     * @ORM\Column(name="Nombre", type="integer")
     */
    private $item;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set player
     *
     * @param Player $user
     */
    public function setPlayer(Player $player)
    {
        $this->player = $player;
    }

    /**
     * Get player
     *
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set action
     *
     * @param integer $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * Get action
     *
     * @return integer 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set item
     *
     * @param integer $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * Get item
     *
     * @return integer 
     */
    public function getItem()
    {
        return $this->item;
    }
}