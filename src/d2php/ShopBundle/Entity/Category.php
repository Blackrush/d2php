<?php

namespace d2php\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\ShopBundle\Entity\Category
 *
 * @ORM\Table(name="d2php_gift_categories")
 * @ORM\Entity(repositoryClass="d2php\ShopBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    
    /**
     * @var \Doctrine\Common\Collections\Collection $gifts
     * 
     * @ORM\OneToMany(targetEntity="d2php\ShopBundle\Entity\Gift", mappedBy="category")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $gifts;
    
    public function __construct(){
    	$gifts = new ArrayCollection;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Add gift
     * 
     * @param Gift $gift
     */
    public function addGift(Gift $gift){
    	$this->gifts[] = $gift;    	
    }
    
    /**
     * Get gifts
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGifts(){
    	return $this->gifts;
    }
}