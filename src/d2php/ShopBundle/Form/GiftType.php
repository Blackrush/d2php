<?php

namespace d2php\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class GiftType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'Nom du cadeau : '))
            ->add('itemId', 'text', array('label' => 'Id de l\'objet : '))
            ->add('category', 'entity', array(
            		'class' => 'd2phpShopBundle:Category',
            		'property' => 'name',
            		'multiple' => false,
            		'empty_value' => '????????',
            		'label' => 'Catégorie du cadeau : ',
            ))
            ->add('price', 'text', array('label' => 'Prix du cadeau : '))
            ->add('image', 'text', array('label' => 'Url de l\'image du cadeau : ', 'required' => false))
            ->add('description', 'textarea', array('label' => 'Rapide description : '))
        ;
    }

    public function getName()
    {
        return 'd2php_shopbundle_gifttype';
    }
}
