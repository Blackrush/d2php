<?php

namespace d2php\ShopBundle\Form;

use d2php\UserBundle\Entity\User;

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\AbstractType;

class GiftSelectionType extends AbstractType {
	/**
	 * @var array $players
	 */
	private $players;
	
	public function __construct(array $players){
		$this->players = $players;
	}
	
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder
			->add('player', 'choice', array(
					'choices' => $this->players,
					'multiple' => false,
					'empty_value' => '??????',
					'label' => 'Choisissez un personnage : '
			))
			->add('gift', 'entity', array(
					'class' => 'd2phpShopBundle:Gift',
					'multiple' => false,
					'expanded' => true,
					'label' => 'Choisissez un cadeau : ',
			))
		;
	}
	
	public function getName()
	{
		return 'd2php_shopbundle_giftselectiontype';
	}
}