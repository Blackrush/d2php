<?php

namespace d2php\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class PlayerSelectionType extends AbstractType
{
	/**
	 * @var array $players
	 */
	private $players;
	
	public function __construct(array $players){
		$this->players = $players;		
	}
	
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('player', 'choice', array(
        			'choices' => $this->players,
        			'expanded' => false,
        			'multiple' => false,
        			'empty_data' => '???????',
        			'label' => 'Choisissez un personnage : ',
        	))
        ;
    }

    public function getName()
    {
        return 'd2php_shopbundle_playerselectiontype';
    }
}
