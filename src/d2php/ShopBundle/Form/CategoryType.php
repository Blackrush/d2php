<?php

namespace d2php\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'Nom de la catégorie : '))
            ->add('image', 'text', array('label' => 'Image de la catégorie : '))
            ->add('description', 'textarea', array('label' => 'Description de la catégorie : '))
        ;
    }

    public function getName()
    {
        return 'd2php_shopbundle_categorytype';
    }
}
