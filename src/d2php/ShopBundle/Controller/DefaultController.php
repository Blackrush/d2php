<?php

namespace d2php\ShopBundle\Controller;

use d2php\ShopBundle\Entity\BuyPointsHistory;

use d2php\CmsBundle\Helper\d2phpController;
use d2php\ShopBundle\Entity\Gift;
use d2php\ShopBundle\Form\GiftType;

class DefaultController extends d2phpController {
	public function buyAction(){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return $this->redirect('/');
		}
		
		return $this->render('d2phpShopBundle:Default:buy.html.twig', array(
				'starpass_id' => $this->getParameter('starpass_idd'),
				'ogrines' => $this->getParameter('nb_points'),
		));
	}
	
	public function buyConfirmationAction(){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return $this->redirect('/');
		}
		
		$ident=$idp=$ids=$idd=$codes=$code1=$code2=$code3=$code4=$code5=$datas='';
		$idp = $this->getParameter('starpass_idp');
		$idd = $this->getParameter('starpass_idd');
		$ident=$idp.";".$ids.";".$idd;
		
		$code1 = $this->getRequest()->request->get('code1', '');
		$code2 = $this->getRequest()->request->get('code2', '');
		$code3 = $this->getRequest()->request->get('code3', '');
		$code4 = $this->getRequest()->request->get('code4', '');
		$code5 = $this->getRequest()->request->get('code5', '');
		$codes=$code1.$code2.$code3.$code4.$code5;
		
		$datas = $this->getRequest()->request->get('DATAS', '');
		$ident=urlencode($ident);
		$codes=urlencode($codes);
		$datas=urlencode($datas);
		
		$get_f=@file('http://script.starpass.fr/check_php.php?ident='.$ident.'&codes='.$codes.'&DATAS='.$datas);
		if(!$get_f)
		{
			return new Response(
					"Votre serveur n'a pas accès au serveur de Starpass, merci de contacter votre hébergeur.",
					500
			);
		}
		$tab = explode("|",$get_f[0]);
		
		if(!$tab[1]) $url = "";
		else $url = $tab[1];
		
		$pays = $tab[2];
		$palier = urldecode($tab[3]);
		$id_palier = urldecode($tab[4]);
		$type = urldecode($tab[5]);
		
		if(substr($tab[0],0,3) != "OUI") {
			return $this->redirect($url);
		}
		else {
			$this->getRequest()->cookies->set('CODE_BON', '1');
			
			$points = $this->getParameter('nb_points');
			$user = $this->getUser();
			$user->addPoints($points);
			
			$history = new BuyPointsHistory;
			$history->setBuyer($user);
			$history->setQuantity($points);
			$history->setDate(new \DateTime);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($history);
			$em->persist($user);
			$em->flush();
			return $this->render('d2phpShopBundle:Default:buy_confirmation.html.twig', array(
					'points' => $points
			));
		}
	}
	
	public function buyErrorAction(){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return $this->redirect('/');
		}
		
		return $this->render('d2phpShopBundle:Default:buy_error.html.twig');
	}
	
	public function voteAction(){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return $this->redirect($this->getParameter('lien_vote'));
		}
		
		$vote_interval = new \DateInterval('PT'.$this->getParameter('vote_interval').'H');
		
		$user = $this->getUser();

		$now = new \DateTime;
		if ($user->getLastVote()) {
		    $required = $user->getLastVote()->add($vote_interval);
		    $valid = $now >= $required;
		}
		else{
		    $valid = true;
		}
		
		if (!$valid){
			return $this->render('d2phpShopBundle:Default:vote_error.html.twig', array(
					'required' => $now->diff($required)
			));
		}
		else{
			$user->addPoints($this->getParameter('nb_points_vote'));
			$user->setLastVote($now);
			$user->incrementTimesVoted();
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($user);
			$em->flush();
			
			return $this->redirect($this->getParameter('lien_vote'));
		}
	}
	
	public function addGiftAction(){
		$gift = new Gift;
		$form = $this->createForm(new GiftType, $gift);
		
		if ($this->isPostAction($form) && $form->isValid()){
			$gift->setQuantity(1);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($gift);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_shop_gifts'));
		}
		
		return $this->render('d2phpShopBundle:Gift:add.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function editGiftAction(Gift $gift){
		$form = $this->createForm(new GiftType, $gift);
		
		if ($this->isPostAction($form) && $form->isValid()){			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($gift);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_shop_gifts'));
		}
		
		return $this->render('d2phpShopBundle:Gift:edit.html.twig', array(
				'gift' => $gift,
				'form' => $form->createView()
		));
	}
	
	public function deleteGiftAction(Gift $gift){
		if ($this->getRequest()->query->get('confirmation', false)){		
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($gift);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_shop_gifts'));
		}
		
		return $this->render('d2phpShopBundle:Gift:delete.html.twig', array(
				'gift' => $gift
		));
	}
	
	public function giftsAction(){
		$gifts = $this->getDoctrine()->getRepository('d2phpShopBundle:Gift')->findAll();
		return $this->render('d2phpShopBundle:Gift:gifts.html.twig', array(
				'gifts' => $gifts
		));
	}
}
