<?php

namespace d2php\ShopBundle\Controller;

use d2php\CmsBundle\Helper\d2phpController;
use d2php\ShopBundle\Entity\Category;
use d2php\ShopBundle\Form\CategoryType;

class CategoryController extends d2phpController {
	public function viewAction(Category $category){
		return $this->render('d2phpShopBundle:Category:view.html.twig', array(
				'category' => $category
		));	
	}
	
	public function addAction(){
		$category = new Category;
		$form = $this->createForm(new CategoryType, $category);

		if ($this->isPostAction($form) && $form->isValid()){
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($category);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_shop_gifts'));
		}
		
		return $this->render('d2phpShopBundle:Category:add.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function editAction(Category $category){
		$form = $this->createForm(new CategoryType, $category);

		if ($this->isPostAction($form) && $form->isValid()){
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($category);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_shop_gifts'));
		}
		
		return $this->render('d2phpShopBundle:Category:edit.html.twig', array(
				'category' => $category,
				'form' => $form->createView()
		));
	}
	
	public function deleteAction(Category $category){
		if ($this->getRequest()->query->get('confirmation', false)){
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($category);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_shop_gifts'));
		}

		return $this->render('d2phpShopBundle:Category:delete.html.twig', array(
				'category' => $category
		));
	}
}