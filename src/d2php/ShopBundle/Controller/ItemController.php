<?php

namespace d2php\ShopBundle\Controller;

use d2php\ShopBundle\Entity\BuyGiftHistory;

use d2php\CmsBundle\Helper\d2phpController;
use d2php\ShopBundle\Form\PlayerSelectionType;
use d2php\ShopBundle\Entity\Gift;
use d2php\ShopBundle\Entity\Category;
use d2php\ShopBundle\Entity\LiveAction;

class ItemController extends d2phpController {
	public function chooseAction(Category $category){
		return $this->render('d2phpShopBundle:Item:choose.html.twig', array(
				'categories' => $this->getDoctrine()->getRepository('d2phpShopBundle:Category')->findAll(),
				'gifts' => $category->getGifts()
		));
	}
	
	public function buyAction(Gift $gift){
		$user = $this->getUser();
		
		if ($gift->getPrice() > $user->getPoints()){
			return $this->render('d2phpShopBundle:Item:buy_error.html.twig', array(
					'error' => "Vous n'avez pas assez d'ogrines."
			));
		}
		
		$players = $user->getPlayers()->toArray();
		$form = $this->createForm(new PlayerSelectionType($players));

		if ($this->isPostAction($form) && $form->isValid()){
			$data = $form->getData();
			$player = $players[$data['player']];
			
			if (!$user->getConnected()){
				return $this->render('d2phpShopBundle:Item:buy_error.html.twig', array(
						'error' => $player->getName() ." n'est pas connecté."
				));
			}
			
			$action = new LiveAction;
			$action->setPlayer($player);
			$action->setAction(LIVE_ACTION_GIVE_ITEM);
			$action->setItem($gift->getItemId());
			
			$user->addPoints(-$gift->getPrice());
			
			$history = new BuyGiftHistory;
			$history->setBuyer($user);
			$history->setCost($gift->getPrice());
			$history->setDate(new \DateTime);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($action);
			$em->persist($history);
			$em->persist($user);
			$em->flush();
			
			return $this->render('d2phpShopBundle:Item:buy_confirmation.html.twig', array(
					'gift' => $gift,
					'player' => $player,
			));
		}
		
		return $this->render('d2phpShopBundle:Item:buy.html.twig', array(
				'form' => $form->createView(),
				'gift' => $gift,
				'is_connected' => $user->getConnected()
		));
	}
}
