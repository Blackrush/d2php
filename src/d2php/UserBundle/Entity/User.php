<?php

namespace d2php\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Security\Core\User\UserInterface;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * d2php\UserBundle\Entity\User
 *
 * @ORM\Table(name="accounts")
 * @ORM\Entity(repositoryClass="d2php\UserBundle\Entity\UserRepository")
 */
class User implements UserInterface {
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="guid", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string $name
	 *
	 * @ORM\Column(name="account", type="string", length=30)
	 * 
	 * @Assert\NotBlank()
	 * @Assert\MinLength(4)
	 */
	private $name;

	/**
	 * @var string $password
	 *
	 * @ORM\Column(name="pass", type="string", length=50)
	 */
	private $password;

	/**
	 * @var string $nickname
	 *
	 * @ORM\Column(name="pseudo", type="string", length=30)
	 * 
	 * @Assert\NotBlank()
	 * @Assert\MinLength(5)
	 */
	private $nickname;

	/**
	 * @var string $question
	 *
	 * @ORM\Column(name="question", type="string", length=100)
	 * @Assert\MinLength(5)
	 */
	private $question;

	/**
	 * @var string $answer
	 *
	 * @ORM\Column(name="reponse", type="string", length=100)
	 * 
	 * @Assert\NotBlank()
	 * @Assert\MinLength(5)
	 */
	private $answer;

	/**
	 * @var string $email
	 * 
	 * @ORM\Column(name="email", type="string", length=100)
	 * 
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @var integer $rights
	 *
	 * @ORM\Column(name="level", type="integer")
	 */
	private $rights;

	/**
	 * @var boolean $banned
	 *
	 * @ORM\Column(name="banned", type="boolean")
	 */
	private $banned;

	/**
	 * @var datetime $createdAt
	 *
	 * @ORM\Column(name="createdAt", type="datetime")
	 */
	private $createdAt;

	/**
	 * @var boolean $publicEmail
	 * 
	 * @ORM\Column(name="publicEmail", type="boolean")
	 */
	private $publicEmail;

	/**
	 * @var boolean $reloadNeeded
	 * 
	 * @ORM\Column(name="reload_needed", type="boolean")
	 */
	private $reloadNeeded;

	/**
	 * @var boolean $connected
	 * 
	 * @ORM\Column(name="logged", type="boolean")
	 */
	private $connected;

	/**
	 * @var integer $points
	 * 
	 * @ORM\Column(name="points", type="integer")
	 */
	private $points;

	/**
	 * @var \Doctrine\Common\Collections\Collection $players
	 * 
	 * @ORM\OneToMany(targetEntity="d2php\UserBundle\Entity\Player", mappedBy="owner")
	 * @ORM\OrderBy({"level" = "DESC"})
	 */
	private $players;

	/**
	 * @var datetime $lastVote
	 * 
	 * @ORM\Column(name="lastVote", type="datetime", nullable=true)
	 */
	private $lastVote;

	/**
	 * @var integer $timesVoted
	 * 
	 * @ORM\Column(name="times_Voted", type="integer")
	 */
	private $timesVoted;

	public function __construct() {
		$this->players = new ArrayCollection;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set password
	 *
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * Get password
	 *
	 * @return string 
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * Set nickname
	 *
	 * @param string $nickname
	 */
	public function setNickname($nickname) {
		$this->nickname = $nickname;
	}

	/**
	 * Get nickname
	 *
	 * @return string 
	 */
	public function getNickname() {
		return $this->nickname;
	}

	/**
	 * Set question
	 *
	 * @param string $question
	 */
	public function setQuestion($question) {
		$this->question = $question;
	}

	/**
	 * Get question
	 *
	 * @return string 
	 */
	public function getQuestion() {
		return $this->question;
	}

	/**
	 * Set answer
	 *
	 * @param string $answer
	 */
	public function setAnswer($answer) {
		$this->answer = $answer;
	}

	/**
	 * Get answer
	 *
	 * @return string 
	 */
	public function getAnswer() {
		return $this->answer;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set rights
	 *
	 * @param integer $rights
	 */
	public function setRights($rights) {
		$this->rights = $rights;
	}

	/**
	 * Get rights
	 *
	 * @return integer 
	 */
	public function getRights() {
		return $this->rights;
	}

	/**
	 * Set banned
	 *
	 * @param boolean $banned
	 */
	public function setBanned($banned) {
		$this->banned = $banned;
	}

	/**
	 * Get banned
	 *
	 * @return boolean
	 */
	public function getBanned() {
		return $this->banned;
	}

	/**
	 * @param datetime $createdAt
	 */
	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	/**
	 * @return datetime
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 * @param boolean $publicEmail
	 */
	public function setPublicEmail($publicEmail) {
		$this->publicEmail = $publicEmail;
	}

	/**
	 * @return boolean
	 */
	public function getPublicEmail() {
		return $this->publicEmail;
	}

	/**
	 * Set reloadNeed
	 *
	 * @param boolean $reloadNeeded
	 */
	public function setReloadNeeded($reloadNeeded) {
		$this->reloadNeeded = $reloadNeeded;
	}

	/**
	 * Get reloadNeeded
	 *
	 * @return boolean
	 */
	public function getReloadNeeded() {
		return $this->reloadNeeded;
	}

	/**
	 * Set connected
	 *
	 * @param boolean $connected
	 */
	public function setConnected($connected) {
		$this->connected = $connected;
	}

	/**
	 * Get connected
	 *
	 * @return boolean
	 */
	public function getConnected() {
		return $this->connected;
	}

	/**
	 * Set points
	 * 
	 * @param integer $points
	 */
	public function setPoints($points) {
		$this->points = $points;
	}

	/**
	 * Add points
	 * 
	 * @param integer $points
	 */
	public function addPoints($points) {
		$this->points += $points;
	}

	/**
	 * Get points
	 * 
	 * @return integer
	 */
	public function getPoints() {
		return $this->points;
	}

	/**
	 * @param Player $player
	 */
	public function addPlayer(Player $comment) {
		$this->players->add($comment);
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPlayers() {
		return $this->players;
	}

	/**
	 * @param \DateTime $lastVote
	 */
	public function setLastVote(\DateTime $lastVote) {
		$this->lastVote = $lastVote;
	}

	/**
	 * @return \DateTime
	 */
	public function getLastVote() {
		return $this->lastVote;
	}

	public function getTimesVoted() {
		return $this->timesVoted;
	}

	public function setTimesVoted($timesVoted) {
		$this->timesVoted = $timesVoted;
	}
	
	public function incrementTimesVoted() {
		$this->timesVoted++;
	}

	public function getRoles() {
		if ($this->banned) {
			return array('ROLE_BANNED');
		}

		switch ($this->rights) {
		case 0:
			return array('ROLE_MEMBER');

		case 1: //??
		case 2: //quiz master
			return array('ROLE_QUIZ_MASTER');

		case 3: // game master
		case 4: // community manager
			return array('ROLE_GAME_MASTER');

		case 5: // admin
			return array('ROLE_ADMINISTRATOR');
		}
	}

	public function getSalt() {
		return '';
	}

	public function getUsername() {
		return $this->name;
	}

	public function eraseCredentials() {

	}

	public function equals(UserInterface $user) {
		return $user->getUsername() == $this->name;
	}

	public function getRightsStr() {
		if ($this->banned) {
			return 'Bannis';
		}

		switch ($this->rights) {
		case 0:
			return 'Membre';

		case 1:
		case 2:
			return 'Animateur';

		case 3:
			return 'Maître de Jeu';

		case 4:
			return 'Community Manager';

		case 5:
			return 'Administrateur';
		}
	}
}
