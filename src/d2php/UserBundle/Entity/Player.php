<?php

namespace d2php\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\UserBundle\Entity\Player
 *
 * @ORM\Table(name="personnages")
 * @ORM\Entity(repositoryClass="d2php\UserBundle\Entity\PlayerRepository")
 */
class Player
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="guid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var User $owner
     * 
     * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\User", inversedBy="players")
     * @ORM\JoinColumn(name="account", referencedColumnName="guid")
     */
    private $owner;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var boolean $gender
     *
     * @ORM\Column(name="sexe", type="boolean")
     */
    private $gender;

    /**
     * @var integer $breed
     *
     * @ORM\Column(name="class", type="integer")
     */
    private $breed;

    /**
     * @var integer $kamas
     *
     * @ORM\Column(name="kamas", type="integer")
     */
    private $kamas;

    /**
     * @var integer $level
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set owner
     *
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
    	$this->owner = $owner;
    }
    
    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner()
    {
    	return $this->owner;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gender
     *
     * @param boolean $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Get gender
     *
     * @return boolean 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set breed
     *
     * @param integer $breed
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;
    }

    /**
     * Get breed
     *
     * @return integer 
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * Set kamas
     *
     * @param integer $kamas
     */
    public function setKamas($kamas)
    {
        $this->kamas = $kamas;
    }

    /**
     * Get kamas
     *
     * @return integer 
     */
    public function getKamas()
    {
        return $this->kamas;
    }

    /**
     * Set level
     *
     * @param integer $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }
    
    public function breedStr(){
    	switch ($this->breed){
    		case 1:
    			if ($this->gender){
    				return 'Féca';
    			}
    			else{
    				return 'Fécatte';
    			}
    			break;
    			
    		case 2:
    			if ($this->gender){
    				return 'Osamodas (mâle)';
    			}
    			else{
    				return 'Osamodas (femelle)';
    			}
    			
    		case 3:
    			if ($this->gender){
    				return 'Enutrof';
    			}
    			else{
    				return 'Enutrofette';
    			}
    			break;
    			
    		case 4:
    			if ($this->gender){
    				return 'Sram';
    			}
    			else{
    				return 'Sramette';
    			}
    			
    		case 5:
    			if ($this->gender){
    				return 'Xélor';
    			}
    			else{
    				return 'Xélorette';
    			}
    			
    		case 6:
    			if ($this->gender){
    				return 'Ecaflip';
    			}
    			else{
    				return 'Ecaflipette';
    			}
    			
    		case 7:
    			if ($this->gender){
    				return 'Eniripsa (mâle)';
    			}
    			else{
    				return 'Eniripsa (femelle)';
    			}
    		
    		case 8:
    			if ($this->gender){
    				return 'Iop';
    			}
    			else{
    				return 'Iopette';
    			}
    			
    		case 9:
    			if ($this->gender){
    				return 'Crâ';
    			}
    			else{
    				return 'Crâette';
    			}
    			
    		case 10:
    			if ($this->gender){
    				return 'Sadida';
    			}
    			else{
    				return 'Sadidette';
    			}
    			
    		case 11:
    			if ($this->gender){
    				return 'Sacrieur';
    			}
    			else{
    				return 'Sacrieuse';
    			}
    			
    		case 12:
    			if ($this->gender){
    				return 'Pandawa (mâle)';
    			}
    			else{
    				return 'Pandawa (femelle)';
    			}  			
    	}
    }
    
    public function __toString(){
    	return $this->name;
    }
}