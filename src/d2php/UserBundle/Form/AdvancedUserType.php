<?php

namespace d2php\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AdvancedUserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Nom de compte : '))
            ->add('nickname', null, array('label' => 'Pseudo (visible IG) : '))
            ->add('question', null, array('label' => 'Question secrète : '))
            ->add('answer', null, array('label' => 'Réponse secrète : '))
            ->add('email', 'email', array('label' => 'Adresse email : '))
            ->add('rights', 'choice', array(
            		'choices' => array(
            				'0' => 'Membre',
            				'2' => 'Animateur',
            				'3' => 'Maître de Jeu',
            				'4' => 'Administrateur'
            		),
            		'label' => 'Droits : '
           	))
           	->add('banned', 'checkbox', array(
           			'label' => 'Bannis ? ',
           			'required' => false
           	))
            ->add('publicEmail', 'checkbox', array(
            		'label' => 'Afficher publiquement l\'adresse email ? ',
            		'required' => false
            ))
        ;
    }

    public function getName()
    {
        return 'd2php_userbundle_advancedusertype';
    }
}
