<?php

namespace d2php\UserBundle\Form;

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\AbstractType;

class EditPasswordType extends AbstractType {
    public function buildForm(FormBuilder $builder, array $options)
    {
    	$builder
    		->add('current_password', 'password', array('label' => 'Votre mot de passe actuel : '))
    		->add('new_password', 'repeated', array(
    				'type' => 'password',
    				'first_name' => 'Le nouveau mot de passe : ',
    				'second_name' => 'Confirmation : '
    		))
    	;
    }

    public function getName()
    {
        return 'd2php_userbundle_usertype';
    }
}