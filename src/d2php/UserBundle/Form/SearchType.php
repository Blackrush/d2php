<?php

namespace d2php\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nickname', 'text', array('label' => 'Pseudo : '))
        ;
    }

    public function getName()
    {
        return 'd2php_userbundle_usertype';
    }
}
