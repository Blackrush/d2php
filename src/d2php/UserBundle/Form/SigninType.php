<?php

namespace d2php\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class SigninType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Nom d\'utilisateur : '))
            ->add('password', 'repeated', array(
            		'type' => 'password',
            		'first_name' => 'Mot de passe : ',
            		'second_name' => 'Mot de passe (confirmation) : '
            ))
            ->add('nickname', null, array('label' => 'Pseudo (visible IG) : '))
            ->add('question', null, array('label' => 'Question secrète : '))
            ->add('answer', null, array('label' => 'Réponse secrète : '))
            ->add('email', null, array('label' => 'Adresse email : '))
        ;
    }

    public function getName()
    {
        return 'd2php_userbundle_signintype';
    }
}
