<?php

namespace d2php\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
    	$builder
	    	->add('email', 'email', array('label' => 'Adresse email : '))
	    	->add('publicEmail', 'checkbox', array(
	    			'label' => 'Afficher publiquement l\'adresse email ? ',
	    			'required' => false
	    	))
    	;
    }

    public function getName()
    {
        return 'd2php_userbundle_usertype';
    }
}
