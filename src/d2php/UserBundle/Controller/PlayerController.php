<?php

namespace d2php\UserBundle\Controller;

use d2php\CmsBundle\Helper\d2phpController;

class PlayerController extends d2phpController {
	public function bestPlayersAction(){
		$players = $this->getDoctrine()->getRepository('d2phpUserBundle:Player')->getBestPlayers();
		
		return $this->render('d2phpUserBundle:Player:best_players.html.twig', array(
				'players' => $players
		));
	}
	
	public function richestPlayersAction(){
		$players = $this->getDoctrine()->getRepository('d2phpUserBundle:Player')->getRichestPlayers();
		
		return $this->render('d2phpUserBundle:Player:richest_players.html.twig', array(
				'players' => $players
		));
	}
}