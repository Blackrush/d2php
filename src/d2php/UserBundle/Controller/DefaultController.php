<?php

namespace d2php\UserBundle\Controller;

use d2php\UserBundle\Form\EditPasswordType;

use Symfony\Component\Form\FormError;

use d2php\UserBundle\d2phpUserBundle;

use Symfony\Component\Form\Extension\Core\Type\SearchType;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;

use d2php\UserBundle\Entity\User;
use d2php\UserBundle\Form\SigninType;
use d2php\UserBundle\Form\AdvancedUserType;
use d2php\UserBundle\Form\UserType;
use d2php\CmsBundle\Helper\d2phpController;

class DefaultController extends d2phpController {	
	public function loginAction(){
		$request = $this->getRequest();
		$session = $request->getSession();
		
		// get the login error if there is one
		if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
		} else {
			$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
		}
		
		return $this->render('d2phpUserBundle:Default:login.html.twig', array(
				'last_username' => $session->get(SecurityContext::LAST_USERNAME),
				'error'         => $error,
		));
	}
	
	public function signinAction(){
		$user = new User;
		$form = $this->createForm(new SigninType, $user);	
		
		if ($this->isPostAction($form) && $form->isValid()){
			$user->setConnected(false);
			$user->setReloadNeeded(true);
			$user->setRights(0);
			$user->setBanned(false);
			$user->setCreatedAt(new \DateTime);
			$user->setPublicEmail(false);
			$user->setPoints(0);
			$user->setTimesVoted(0);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($user);
			$em->flush();
			
			return $this->render('d2phpUserBundle:Default:signin_confirmation.html.twig');
		}
		
		return $this->render('d2phpUserBundle:Default:signin.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function viewAction(User $user){
		return $this->render('d2phpUserBundle:Default:view.html.twig', array(
				'user' => $user
		)); 
	}
	
	public function editAction(){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return new AccessDeniedException;
		}
		
		$user = $this->getUser();
		$password = $user->getPassword();
		$form = $this->createForm(new UserType, $user);
		
		if ($this->isPostAction($form) && $form->isValid()){
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($user);
			$em->flush();

			return $this->redirect($this->generateUrl('d2php_user_view', array(
					'id' => $user->getId()
			)));
		}
		
		return $this->render('d2phpUserBundle:Default:edit.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function adminEditAction(User $user){
		$form = $this->createForm(new AdvancedUserType, $user);
		$password = $user->getPassword();
		
		if ($this->isPostAction($form) && $form->isValid()){
			$user->setReloadNeeded(true);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($user);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_user_view', array(
					'id' => $user->getId()
			)));
		}
		
		return $this->render('d2phpUserBundle:Default:admin_edit.html.twig', array(
				'user' => $user,
				'form' => $form->createView()
		));
	}
	
	public function deleteAction(User $user){
		if ($this->getRequest()->query->get('confirmation', false) == '1'){
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($user);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_admin'));
		}
		
		return $this->render('d2phpUserBundle:Default:delete.html.twig', array(
				'user' => $user
		));
	}
	
	public function banAction(User $user){
		if ($this->getRequest()->query->get('confirmation', false) == '1'){
			$user->setBanned(true);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($user);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_admin'));
		}
		
		return $this->render('d2phpUserBundle:Default:ban.html.twig', array(
				'user' => $user
		));
	}
	
	public function searchAction(){
		$form = $this->createFormBuilder()
			->add('nickname', 'text', array('label' => 'Pseudo : '))
			->getForm();

		if ($this->isPostAction($form) && $form->isValid()){
			$data = $form->getData();
			$nickname = $data['nickname'];
			if (strlen($nickname) > 3){
				$results = $this->getDoctrine()->getRepository('d2phpUserBundle:User')->search($nickname);
			}
			else{
				$results = array();
				$form->addError(new FormError('Veuillez entrer au minimum 4 caractères.'));
			}
		}
		else{
			$results = array();
		}
		
		return $this->render('d2phpUserBundle:Default:search.html.twig', array(
				'results' => $results,
				'form' => $form->createView()
		));
 	}
 	
 	public function editPasswordAction(){
 		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
 			return new AccessDeniedException;
 		}
 		
 		$user = $this->getUser();
 		$form = $this->createForm(new EditPasswordType);
 		
 		if ($this->isPostAction($form) && $form->isValid()){
 			$data = $form->getData();
 			if ($user->getPassword() == $data['current_password']){
 				$user->setPassword($data['new_password']);
 				$em = $this->getDoctrine()->getEntityManager();
 				$em->persist($user);
 				$em->flush();
 				
 				return $this->redirect($this->generateUrl('d2php_user_view', array('id' => $user->getId())));
 			}
 			else{
 				$form->get('current_password')->addError(new FormError('Mauvais de passe.'));
 			}
 		}
 		
 		return $this->render('d2phpUserBundle:Default:edit_password.html.twig', array(
 				'form' => $form->createView()
 		));
 	}
 	
 	public function lastAction(){
 		$last = $this->getDoctrine()->getRepository('d2phpUserBundle:User')->getLastUsers();
 		return $this->render('d2phpUserBundle:Default:last.html.twig', array(
 				'last' => $last
 		));
 	}
 	
 	public function bestVotersAction() {
 		$bestVoters = $this->getDoctrine()->getRepository('d2phpUserBundle:User')->getBestVoters();
 		return $this->render('d2phpUserBundle:Default:best_voters.html.twig', array(
 				'best_voters' => $bestVoters
 		));
 	}
}
