<?php

namespace d2php\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('message')
        ;
    }

    public function getName()
    {
        return 'd2php_newsbundle_commenttype';
    }
}
