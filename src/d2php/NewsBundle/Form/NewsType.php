<?php

namespace d2php\NewsBundle\Form;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('title', null, array('label' => 'Titre de la news : '))
            ->add('message', null, array('label' => 'Message de la news : '))
            ->add('category', 'entity', array(
            		'class' => 'd2phpNewsBundle:Category',
            		'property' => 'name',
            		'label' => 'Catégorie de la news : ',
            ))
        ;
    }

    public function getName()
    {
        return 'd2php_newsbundle_newstype';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
    			'data_class' => 'd2php\NewsBundle\Entity\News',
    	);
    }
}
