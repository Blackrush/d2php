<?php

namespace d2php\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Nom de la catégorie : '))
        ;
    }

    public function getName()
    {
        return 'd2php_newsbundle_categorytype';
    }
}
