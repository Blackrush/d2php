<?php

namespace d2php\NewsBundle\Controller;

use d2php\CmsBundle\Helper\Paginator;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;

use d2php\NewsBundle\Entity\News;
use d2php\NewsBundle\Entity\Comment;
use d2php\NewsBundle\Entity\Category;
use d2php\NewsBundle\Form\NewsType;
use d2php\NewsBundle\Form\CategoryType;
use d2php\CmsBundle\Helper\d2phpController;

define('MAX_COMMENTS_PER_PAGE', 25);

class DefaultController extends d2phpController {	
	public function viewAction(News $news){
		$comments = new Paginator($news->getComments(), MAX_COMMENTS_PER_PAGE, $this->getRequest()->query->get('page', 1));
		return $this->render('d2phpNewsBundle:Default:view.html.twig', array(
				'news' => $news,
				'comments' => $comments
		));
	}	
	
	public function addAction(){
		$news = new News;
		$form = $this->createForm(new NewsType, $news);
		
		if ($this->isPostAction($form) && $form->isValid()){
			$news->setAuthor($this->getUser());
			$news->setDate(new \DateTime);
			$news->getCategory()->addNews($news);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($news);
			$em->persist($news->getCategory());
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_view', array(
					'id' => $news->getId()
			)));
		}
		
		return $this->render('d2phpNewsBundle:Default:add.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function editAction(News $news){
		$form = $this->createForm(new NewsType, $news);
		
		if ($this->isPostAction($form) && $form->isValid()){
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($news);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_view', array(
					'id' => $news->getId()
			)));
		}
	
		return $this->render('d2phpNewsBundle:Default:edit.html.twig', array(
				'news' => $news,
				'form' => $form->createView()
		));
	}
	
	public function deleteAction(News $news){
		if ($this->getRequest()->query->get('confirmation', false) == '1'){
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($news);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_admin'));
		}
		
		return $this->render('d2phpNewsBundle:Default:delete.html.twig', array(
				'news' => $news
		));
	}
}
