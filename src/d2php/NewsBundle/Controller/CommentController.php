<?php

namespace d2php\NewsBundle\Controller;

use d2php\CmsBundle\Helper\d2phpController;
use d2php\NewsBundle\Form\CommentType;
use d2php\NewsBundle\Entity\Comment;
use d2php\NewsBundle\Entity\News;

class CommentController extends d2phpController {	
	public function addAction(News $news){
		if (!$this->get('security.context')->isGranted('ROLE_MEMBER')){
			return new AccessDeniedException;
		}
		
		$comment = new Comment;
		$form = $this->createForm(new CommentType, $comment);
		
		if ($this->isPostAction($form) && $form->isValid()){
			$comment->setDate(new \DateTime);
			$comment->setNews($news);
			$comment->setAuthor($this->getUser());
			$news->addComment($comment);
			
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($comment);
			$em->persist($news);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_view', array(
					'id' => $news->getId()
			)));
		}
		
		return $this->render('d2phpNewsBundle:Comment:add.html.twig', array(
				'news' => $news,
				'form' => $form->createView()
		));
	}
	
	public function editAction(Comment $comment){
		$form = $this->createForm(new CommentType, $comment);
		
		if ($this->isPostAction($form) && $form->isValid()){
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($comment);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_view', array(
					'id' => $comment->getNews()->getId()
			)));
		}
		
		return $this->render('d2phpNewsBundle:Comment:edit.html.twig', array(
				'comment' => $comment,
				'form' => $form->createView()
		));
	}
	
	public function deleteAction(Comment $comment){
		if ($this->getRequest()->query->get('confirmation', false) == '1'){
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($comment);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_view', array(
					'id' => $comment->getNews()->getId()
			)));
		}
		
		return $this->render('d2phpNewsBundle:Comment:delete.html.twig', array(
				'comment' => $comment
		));
	}
}