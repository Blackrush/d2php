<?php

namespace d2php\NewsBundle\Controller;

use d2php\CmsBundle\Helper\d2phpController;
use d2php\NewsBundle\Form\CategoryType;
use d2php\NewsBundle\Entity\Category;

class CategoryController extends d2phpController {	
	public function detailsAction(Category $category){
		return $this->render('d2phpNewsBundle:Category:details.html.twig', array(
				'category' => $category,
				'news' => $category->getNews()
		));
	}
	
	public function viewAction(){
		$categories = $this->getDoctrine()->getRepository('d2phpNewsBundle:Category')->findAll();
		return $this->render('d2phpNewsBundle:Category:view.html.twig', array(
				'categories' => $categories
		));
	}
	
	public function addAction(){
		$category = new Category;
		$form = $this->createForm(new CategoryType, $category);
	
		if ($this->isPostAction($form) && $form->isValid()){
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($category);
			$em->flush();
				
			return $this->redirect($this->generateUrl('d2php_news_category_view'));
		}
	
		return $this->render('d2phpNewsBundle:Category:add.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	public function editAction(Category $category){
		$form = $this->createForm(new CategoryType, $category);
		
		if ($this->isPostAction($form) && $form->isValid()){
			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($category);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_category_view'));
		}
		
		return $this->render('d2phpNewsBundle:Category:edit.html.twig', array(
				'category' => $category,
				'form' => $form->createView()
		));
	}
	
	public function deleteAction(Category $category){
		if ($this->getRequest()->query->get('confirmation', false) == '1'){
			$em = $this->getDoctrine()->getEntityManager();
			$em->remove($category);
			$em->flush();
			
			return $this->redirect($this->generateUrl('d2php_news_category_view'));
		}
		
		return $this->render('d2phpNewsBundle:Category:delete.html.twig', array(
				'category' => $category
		));
	}
}