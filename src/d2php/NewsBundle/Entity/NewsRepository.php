<?php

namespace d2php\NewsBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * NewsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NewsRepository extends EntityRepository
{
	public function findAllDesc(){
		return $this->createQueryBuilder('n')
			->orderBy('n.date', 'DESC')
			
			->getQuery()
			->getResult();
	}
}