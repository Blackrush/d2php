<?php

namespace d2php\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\NewsBundle\Entity\Comment
 *
 * @ORM\Table(name="d2php_comments")
 * @ORM\Entity(repositoryClass="d2php\NewsBundle\Entity\CommentRepository")
 */
class Comment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var d2php\UserBundle\Entity\User $author
     * 
     * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="guid")
     */
    private $author;
    
    /**
     * @var \d2php\NewsBundle\Entity\News $news
     * 
     * @ORM\ManyToOne(targetEntity="d2php\NewsBundle\Entity\News", inversedBy="comments")
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     */
    private $news;

    /**
     * @var text $message
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var datetime $date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param \d2php\UserBundle\Entity\User $author
     */
    public function setAuthor(\d2php\UserBundle\Entity\User $author){
    	$this->author = $author;
    }
    
    /**
     * @return \d2php\UserBundle\Entity\User
     */
    public function getAuthor(){
    	return $this->author;
    }
    
    /**
     * Set news
     * 
     * @param \d2php\NewsBundle\Entity\News $news
     */
    public function setNews(\d2php\NewsBundle\Entity\News $news){
    	$this->news = $news;
    }
    
    /**
     * Get news
     * 
     * @return \d2php\NewsBundle\Entity\News
     */
    public function getNews(){
    	return $this->news;
    }

    /**
     * Set message
     *
     * @param text $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get message
     *
     * @return text 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return datetime 
     */
    public function getDate()
    {
        return $this->date;
    }
}