<?php

namespace d2php\NewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\NewsBundle\Entity\News
 *
 * @ORM\Table(name="d2php_news")
 * @ORM\Entity(repositoryClass="d2php\NewsBundle\Entity\NewsRepository")
 */
class News
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \d2php\UserBundle\Entity\User $author
     * 
     * @ORM\ManyToOne(targetEntity="d2php\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="guid")
     */
    private $author;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var text $message
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var datetime $date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @var \Doctrine\Common\Collections\Collection $comments
     * 
     * @ORM\OneToMany(targetEntity="d2php\NewsBundle\Entity\Comment", mappedBy="news")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $comments;
    
    /**
     * @var \d2php\NewsBundle\Entity\Category $category
     * 
     * @ORM\ManyToOne(targetEntity="d2php\NewsBundle\Entity\Category", inversedBy="news")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    public function __construct(){
    	$this->comments = new ArrayCollection;
    }

    /**
     * @param \d2php\UserBundle\Entity\User $author
     */
    public function setAuthor(\d2php\UserBundle\Entity\User $author){
    	$this->author = $author;
    }
    
    /**
     * @return \d2php\UserBundle\Entity\User
     */
    public function getAuthor(){
    	return $this->author;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param text $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get message
     *
     * @return text 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return datetime 
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * @param \d2php\NewsBundle\Entity\Comment $comment
     */
    public function addComment(\d2php\NewsBundle\Entity\Comment $comment){
    	$this->comments->add($comment);
    }
    
    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments(){
    	return $this->comments;
    }
    
    /**
     * @param \d2php\NewsBundle\Entity\Category $category
     */
    public function setCategory(\d2php\NewsBundle\Entity\Category $category){
    	$this->category = $category;
    }
    
    /**
     * @return \d2php\NewsBundle\Entity\Category
     */
    public function getCategory(){
    	return $this->category;
    }
}