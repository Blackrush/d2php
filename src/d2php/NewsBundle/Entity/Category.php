<?php

namespace d2php\NewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * d2php\NewsBundle\Entity\Category
 *
 * @ORM\Table(name="d2php_categories")
 * @ORM\Entity(repositoryClass="d2php\NewsBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var \Doctrine\Common\Collections\Collection $news
     * 
     * @ORM\OneToMany(targetEntity="d2php\NewsBundle\Entity\News", mappedBy="category")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $news;
    
    public function __construct(){
    	$this->news = new ArrayCollection;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param \d2php\NewsBundle\Entity\News $news
     */
    public function addNews(\d2php\NewsBundle\Entity\News $news){
    	$this->news->add($news);
    }
    
    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews(){
    	return $this->news;
    }
}